package cyberwarriors.hackathon;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import butterknife.BindView;
import butterknife.ButterKnife;
import cyberwarriors.hackathon.Categories.DetailActivity;
import cyberwarriors.hackathon.Categories.ForegroundImageView;
import cyberwarriors.hackathon.Categories.ItemClickSupport;
import cyberwarriors.hackathon.models.Category;
import cyberwarriors.hackathon.setup.Globals;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllTopics extends Fragment {

    private OnFragmentInteractionListener mListener;
    private static final int PHOTO_COUNT = 11;

    RecyclerView grid;
    int columns=3;
    int gridSpacing;
    private PhotoAdapter adapter;
    ProgressBar progressBar;
    public AllTopics() {
        // Required empty public constructor
    }


    public static AllTopics newInstance(String param1, String param2) {
        AllTopics fragment = new AllTopics();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_all_topics, container, false);
        grid = view.findViewById(R.id.image_grid);
        gridSpacing = getResources().getDimensionPixelSize(R.dimen.grid_item_spacing);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), columns);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                /* emulating https://material-design.storage.googleapis.com/publish/material_v_4/material_ext_publish/0B6Okdz75tqQsck9lUkgxNVZza1U/style_imagery_integration_scale1.png */
                switch (position % 6) {
                    case 0:
                    case 1:
                    case 2:
                    case 4:
                        return 1;
                    case 3:
                        return 2;
                    default:
                        return 3;
                }
            }
        });
        progressBar = view.findViewById(R.id.progressLoadingBar);
        grid.setLayoutManager(gridLayoutManager);
        grid.addItemDecoration(new GridMarginDecoration(gridSpacing));
        grid.setHasFixedSize(true);
        ItemClickSupport.addTo(grid).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View view) {
                        Category photo = adapter.getItem(position);
                        Intent intent = new Intent(getContext(), DetailActivity.class);
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(Globals.REST_SERVER + photo.logo));
                        intent.putExtra("category_id",photo.id_category);
                        intent.putExtra("name",photo.name);
                        AllTopics.this.startActivity(intent,
                                ActivityOptions.makeSceneTransitionAnimation(getActivity(), view,
                                        view.getTransitionName()).toBundle());
                    }
                });
        Category.getCategories(new Callback<RestResponse<Category>>() {
            @Override
            public void onResponse(Call<RestResponse<Category>> call, Response<RestResponse<Category>> response) {
                progressBar.setVisibility(View.GONE);
                grid.setVisibility(View.VISIBLE);
                List<Category> categories = response.body().category;
                adapter = new PhotoAdapter(categories.subList(categories.size() - PHOTO_COUNT, categories
                        .size()));
                grid.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<RestResponse<Category>> call, Throwable t) {

            }
        });
        return view;

    }
    static class PhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.photo)
        ForegroundImageView imageView;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private static class GridMarginDecoration extends RecyclerView.ItemDecoration {

        private int space;

        public GridMarginDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.top = space;
            outRect.right = space;
            outRect.bottom = space;
        }
    }

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoViewHolder> {

        private final List<Category> categories;

        public PhotoAdapter(List<Category> photos) {
            this.categories = photos;
        }

        @Override
        public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new PhotoViewHolder(
                    LayoutInflater.from(getContext())
                            .inflate(R.layout.photo_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final PhotoViewHolder holder, final int position) {
            final Category CategoryPhoto = categories.get(position);
            String url = Globals.REST_SERVER + CategoryPhoto.logo;
            Picasso.with(getContext())
                    .load(url)
                    .into(holder.imageView);
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public Category getItem(int position) {
            return categories.get(position);
        }
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
