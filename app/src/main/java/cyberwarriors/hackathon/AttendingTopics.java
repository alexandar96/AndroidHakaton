package cyberwarriors.hackathon;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import cyberwarriors.hackathon.adapter.AttendanceAdapter;
import cyberwarriors.hackathon.models.Attendance;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static cyberwarriors.hackathon.MainActivity.username;

public class AttendingTopics extends Fragment {
    private static final String USER_DATA = "userData";
    private OnFragmentInteractionListener mListener;
    private String userId;
    public AttendingTopics() {
        // Required empty public constructor
    }

    public static AttendingTopics newInstance(String param1, String param2) {
        AttendingTopics fragment = new AttendingTopics();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        userId = this.getActivity().getSharedPreferences(USER_DATA, MODE_PRIVATE).getString("userId",null);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attending_topics, container, false);
        final ListView topicList = view.findViewById(R.id.topic_attendance_list);
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        Resources res = getResources();
        String text = String.format(res.getString(R.string.welcome_messages), username);
        TextView ListHeader = view.findViewById(R.id.topicAttendanceTxtView);
        ListHeader.setText(text);

        Attendance.loadTopicAttendance(userId, new Callback<RestResponse<Attendance>>() {
            @Override
            public void onResponse(Call<RestResponse<Attendance>> call, Response<RestResponse<Attendance>> response) {
                RestResponse<Attendance> teams = response.body();
                AttendanceAdapter arrayAdapter = new AttendanceAdapter(getContext(),0,teams.topics);
                topicList.setAdapter(arrayAdapter);
                arrayAdapter.notifyDataSetChanged();
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<RestResponse<Attendance>> call, Throwable t) {

            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
