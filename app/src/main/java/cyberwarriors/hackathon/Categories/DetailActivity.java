package cyberwarriors.hackathon.Categories;

import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import cyberwarriors.hackathon.R;
import cyberwarriors.hackathon.adapter.AttendanceAdapter;
import cyberwarriors.hackathon.adapter.TopicAdapter;
import cyberwarriors.hackathon.models.Attendance;
import cyberwarriors.hackathon.models.Lesson;
import cyberwarriors.hackathon.models.Topic;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_AUTHOR = "EXTRA_AUTHOR";

    @BindView(R.id.app_bar)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.photo)
    ImageView imageView;
    @BindView(R.id.topicsInCategoryList)
    ListView topicList;
    @BindInt(R.integer.detail_desc_slide_duration) int slideDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Picasso.with(this)
                .load(getIntent().getData())
                .placeholder(R.color.placeholder)
                .into(imageView);

        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout))
                .setTitle(getIntent().getStringExtra("name"));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAfterTransition();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide(Gravity.BOTTOM);
            //slide.addTarget(R.id.description);
            slide.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.interpolator
                    .linear_out_slow_in));
            slide.setDuration(slideDuration);
            getWindow().setEnterTransition(slide);
        }
        Topic.loadTopicsFromCategory(getIntent().getStringExtra("category_id"),new Callback<RestResponse<Topic>>() {
            @Override
            public void onResponse(Call<RestResponse<Topic>> call, Response<RestResponse<Topic>> response) {
                List<Topic> topics = new ArrayList<>();
                TopicAdapter arrayAdapter;
                if (response.body().topics!=null && !response.body().topics.isEmpty()) {
                    topics = response.body().topics;
                    arrayAdapter = new TopicAdapter(getBaseContext(), 0, topics);
                    topicList.setAdapter(arrayAdapter);
                    arrayAdapter.notifyDataSetChanged();
                } else {
                    topics.add(new Topic("N/A",""));
                    arrayAdapter = new TopicAdapter(getBaseContext(), 0, topics);
                    topicList.setAdapter(arrayAdapter);
                    LayoutInflater mInflater=getLayoutInflater();
                    LinearLayout noData=(LinearLayout) mInflater.inflate(R.layout.no_data_avaiable_footer,null);
                    topicList.addFooterView(noData);

                }
            }

            @Override
            public void onFailure(Call<RestResponse<Topic>> call, Throwable t) {

            }
        });
    }
}
