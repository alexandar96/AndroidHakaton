package cyberwarriors.hackathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import cyberwarriors.hackathon.models.User;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginScreen extends AppCompatActivity {
    private static final String USER_DATA = "userData";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        final CheckBox rememberMe = findViewById(R.id.rememberMe);

        Button login = findViewById(R.id.btn_login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText username, password;
                username = findViewById(R.id.input_email);
                password = findViewById(R.id.input_password);

                User.login(username.getText().toString(),password.getText().toString(),new Callback<RestResponse<User>>() {
                    @Override
                    public void onResponse(Call<RestResponse<User>> call, Response<RestResponse<User>> response) {

                        Log.d("Response: ", " " + response.body().getData());
                        if (!response.body().getData().getId().equals("")) {
                            SharedPreferences.Editor editor = getSharedPreferences(USER_DATA, MODE_PRIVATE).edit();
                            editor.putString("userId", response.body().getData().getId());
                            editor.putString("username", username.getText().toString());

                            if(rememberMe.isChecked()) {
                                editor.putString("password", password.getText().toString());
                            }
                            editor.apply();
                            Intent intent = new Intent(LoginScreen.this,MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<RestResponse<User>> call, Throwable t) {
                        Log.d("Throwable", t.getMessage());
                    }
                });
            }
        });
    }
}
