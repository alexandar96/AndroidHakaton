package cyberwarriors.hackathon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cyberwarriors.hackathon.models.User;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    private static final String USER_DATA = "userData";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        String saved_username = getSharedPreferences(USER_DATA,MODE_PRIVATE).getString("username",null);
        String saved_password = getSharedPreferences(USER_DATA,MODE_PRIVATE).getString("password",null);
        final String saved_id = getSharedPreferences(USER_DATA,MODE_PRIVATE).getString("userId",null);
        if(saved_username!=null && saved_password!=null) {
            User.login(saved_username, saved_password, new Callback<RestResponse<User>>() {
                @Override
                public void onResponse(Call<RestResponse<User>> call, Response<RestResponse<User>> response) {
                    Intent intent;
                    if (response.body().getData().getId().equals(saved_id)) {
                        intent = new Intent(SplashScreen.this, MainActivity.class);
                    } else {
                        intent = new Intent(SplashScreen.this, LoginScreen.class);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onFailure(Call<RestResponse<User>> call, Throwable t) {

                }
            });
        } else {
            Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        }
    }
}
