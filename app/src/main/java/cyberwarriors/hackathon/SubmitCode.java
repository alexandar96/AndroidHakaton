package cyberwarriors.hackathon;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Path;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import cyberwarriors.hackathon.models.Lesson;
import cyberwarriors.hackathon.models.Topic;
import cyberwarriors.hackathon.setup.Rest;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubmitCode extends Fragment{

    private OnFragmentInteractionListener mListener;
    static boolean isIpValid = false;
    private ArrayAdapter topicsAdapter;
    private ArrayAdapter lessonsAdapter;
    private List<Topic> topics;
    private List<Lesson> lessons;
    Button submitButton;
    private static final String USER_DATA = "userData";
    private static String detectedIp = null;
    Spinner lessonSpinner;
    public SubmitCode() {
        // Required empty public constructor
    }

    public static SubmitCode newInstance(String param1, String param2) {
        SubmitCode fragment = new SubmitCode();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_submit_code, container, false);

        final Spinner topicSpinner =  view.findViewById(R.id.topicSpinner);
        lessonSpinner = view.findViewById(R.id.lessonSpinner);
        submitButton = view.findViewById(R.id.submitCode);
        final TextInputLayout codeInputField = view.findViewById(R.id.codeInputLabel);
        final EditText InputField = view.findViewById(R.id.codeInputField);
        final TextView CodeStatus = view.findViewById(R.id.codeSubmitStatus);
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        Topic.loadTopics(new Callback<RestResponse<Topic>>() {
            @Override
            public void onResponse(Call<RestResponse<Topic>> call, Response<RestResponse<Topic>> response) {
                if (response.body() != null) {
                    topics = response.body().topics;
                    topics.add(0, new Topic("none", "Odaberite predavanje"));
                    topicsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, topics);
                    topicSpinner.setAdapter(topicsAdapter);

                    if(progressDialog.isShowing())
                        progressDialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<RestResponse<Topic>> call, Throwable t) {

            }
        });

        topicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               if(i!=0) {
                   progressDialog.show();
                   lessonSpinner.setVisibility(View.VISIBLE);
                   Lesson.loadLessonsBy(topics.get(i).getId(),new Callback<RestResponse<Lesson>>() {
                       @Override
                       public void onResponse(Call<RestResponse<Lesson>> call, Response<RestResponse<Lesson>> response) {
                        if(response.body()!=null){
                               lessons = response.body().lessons;
                               lessons.add(0, new Lesson("none", "Odaberite lekciju"));
                               lessonsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, lessons);
                               lessonSpinner.setAdapter(lessonsAdapter);
                               submitButton.setVisibility(View.VISIBLE);
                               codeInputField.setVisibility(View.VISIBLE);
                               submitButton.setEnabled(false);
                               if(progressDialog.isShowing())
                                   progressDialog.dismiss();
                           }
                       }

                       @Override
                       public void onFailure(Call<RestResponse<Lesson>> call, Throwable t) {

                       }
                   });

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        lessonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                       if(InputField.length()>3 && i!=0) {
                                                           submitButton.setEnabled(true);
                                                       } else {
                                                           submitButton.setEnabled(false);
                                                       }
                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                                    }
                                                }

        );
        InputField.addTextChangedListener(mTextWatcher);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog pDialog = new ProgressDialog(getContext());
                pDialog.setMessage("Slanje koda");
                pDialog.setCancelable(false);
                pDialog.show();
                Lesson.submitAttendanceCode(
                        getActivity().getSharedPreferences(USER_DATA,Context.MODE_PRIVATE).getString("userId","N/A"),
                        lessons.get(lessonSpinner.getSelectedItemPosition()).getId(),
                        detectedIp,
                        InputField.getText().toString(),
                        new Callback<RestResponse<Lesson>>() {

                    @Override
                    public void onResponse(Call<RestResponse<Lesson>> call, Response<RestResponse<Lesson>> response) {
                        if(pDialog.isShowing())
                            pDialog.dismiss();
                        switch(response.body().getData().getStatus())
                        {
                            case "success":
                                CodeStatus.setText("Uspešno prijavljeno pristustvo na lekciji");
                                CodeStatus.setTextColor(getResources().getColor(R.color.codeSuccess));
                                break;
                            case "wrongIp":
                                CodeStatus.setText("Neispravna IP adresa. Da li ste povezani na Wireless mrezu StartIt centra?");
                                CodeStatus.setTextColor(getResources().getColor(R.color.codeBadIp));
                                break;
                            case "failure":
                                CodeStatus.setText("Došlo je do greške prilikom evidencije prisustva");
                                CodeStatus.setTextColor(getResources().getColor(R.color.codeFailure));
                                break;
                                default:break;
                        }

                    }

                    @Override
                    public void onFailure(Call<RestResponse<Lesson>> call, Throwable t) {

                    }
                } );
            }
        });
        return  view;

    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // check Fields For Empty Values
            if (editable.length() > 3 && lessonSpinner.getSelectedItemPosition() != 0) {
                submitButton.setEnabled(true);
            } else {
                submitButton.setEnabled(false);
            }
        }
    };

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        isIpValid = false;
        new CheckIP().execute();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
    private  static class CheckIP extends AsyncTask<Void,Void,Void> {
        String ip;
        @Override
        protected Void doInBackground(Void... params) {

            URL whatismyip = null;
            try {
                whatismyip = new URL("http://icanhazip.com/");

                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            whatismyip.openStream()));
                    ip = in.readLine(); //you get the IP as a String

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            detectedIp = ip;

        }

    }
}
