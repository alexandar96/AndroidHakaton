package cyberwarriors.hackathon.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import cyberwarriors.hackathon.R;
import cyberwarriors.hackathon.models.Attendance;

public class AttendanceAdapter extends ArrayAdapter<Attendance> {
    private static LayoutInflater inflater = null;
    private List<Attendance> topiclist;

    public AttendanceAdapter(Context activity, int textViewResourceId, List<Attendance> projectList) {
        super(activity, textViewResourceId, projectList);
        try {
            this.topiclist = projectList;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {
            Log.getStackTraceString(e);
        }
    }

    public int getCount() {
        return topiclist.size();
    }

    public Attendance getItem(int position) {
        return topiclist.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                view = inflater.inflate(R.layout.topic_list_row, parent, false);
                holder = new ViewHolder();
                holder.topicName = view.findViewById(R.id.topicName);
                holder.topicPoints = view.findViewById(R.id.topicPoints);
                holder.topicLogo = view.findViewById(R.id.topicLogo);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.topicName.setText(topiclist.get(position).getName());
            holder.topicPoints.setText(topiclist.get(position).getPoints());

        } catch (Exception e) {
            Log.getStackTraceString(e);
        }
        return view;
    }

    public static class ViewHolder {
        public TextView topicName;
        public TextView topicPoints;
        public ImageView topicLogo;
    }
}
