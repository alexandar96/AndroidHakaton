package cyberwarriors.hackathon.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cyberwarriors.hackathon.R;
import cyberwarriors.hackathon.models.Topic;

/**
 * Created by Aleksandar on 12/11/2017.
 */

public class TopicAdapter extends ArrayAdapter<Topic> {
    private static LayoutInflater inflater = null;
    private List<Topic> topiclist;

    public TopicAdapter(Context activity, int textViewResourceId, List<Topic> projectList) {
        super(activity, textViewResourceId, projectList);
        try {
            this.topiclist = projectList;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {
            Log.getStackTraceString(e);
        }
    }

    public int getCount() {
        return topiclist.size();
    }

    public Topic getItem(int position) {
        return topiclist.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                view = inflater.inflate(R.layout.topics_in_category, parent, false);
                holder = new ViewHolder();
                holder.topicName = view.findViewById(R.id.topicsInCategory);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.topicName.setText(topiclist.get(position).getTopicName());
        } catch (Exception e) {
            Log.getStackTraceString(e);
        }
        return view;
    }

    public static class ViewHolder {
        public TextView topicName;
    }
}