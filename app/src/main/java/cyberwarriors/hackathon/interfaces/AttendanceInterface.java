package cyberwarriors.hackathon.interfaces;

import cyberwarriors.hackathon.models.Attendance;
import cyberwarriors.hackathon.models.User;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AttendanceInterface {

    @GET("listTopicAttendance.php")
    Call<RestResponse<Attendance>> create(@Query("id") String user_Id);


}
