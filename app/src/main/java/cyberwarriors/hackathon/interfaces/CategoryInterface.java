package cyberwarriors.hackathon.interfaces;

import cyberwarriors.hackathon.models.Category;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Aleksandar on 12/11/2017.
 */

public interface CategoryInterface {

    @GET("allCategories.php")
    Call<RestResponse<Category>> list();
}
