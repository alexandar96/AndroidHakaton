package cyberwarriors.hackathon.interfaces;


import cyberwarriors.hackathon.models.Attendance;
import cyberwarriors.hackathon.models.Lesson;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LessonInterface {

    @GET("listLessonsByTopic.php")
    Call<RestResponse<Lesson>> listLesson(@Query("id_topic") String id);

    @GET("checkCode.php")
    Call<RestResponse<Lesson>> submitCode(@Query("id") String id,
                                              @Query("id_lesson") String lessonId,
                                              @Query("ip_address") String Ip,
                                              @Query("code") String code);

}
