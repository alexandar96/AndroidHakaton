package cyberwarriors.hackathon.interfaces;

import cyberwarriors.hackathon.models.Topic;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TopicInterface {

    @GET("listAllTopics.php")
    Call<RestResponse<Topic>> list();

    @GET("listTopicsForCategory.php")
    Call<RestResponse<Topic>> listFromCategory(@Query("category_id") String id);
}
