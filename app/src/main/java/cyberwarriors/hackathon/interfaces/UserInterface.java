package cyberwarriors.hackathon.interfaces;

import cyberwarriors.hackathon.models.User;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserInterface {

    @GET("phoneLogin.php")
    Call<RestResponse<User>> create(@Query("username") String user, @Query("password") String password );

}
