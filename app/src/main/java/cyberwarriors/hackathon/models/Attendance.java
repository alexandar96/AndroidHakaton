package cyberwarriors.hackathon.models;

import cyberwarriors.hackathon.interfaces.AttendanceInterface;
import cyberwarriors.hackathon.setup.Rest;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class Attendance {

    private String id_topic;

    private String name;

    private String logo;

    private String attendance;

    public String getId_topic(){return id_topic;}

    public String getName(){return name;}

    public String getLogo(){return logo;}

    public String getPoints(){return attendance;}


    public static void loadTopicAttendance(String userId, Callback<RestResponse<Attendance>> callback)
    {
        Retrofit retrofit = Rest.getRetrofit();
        AttendanceInterface service = retrofit.create(AttendanceInterface.class);
        Call<RestResponse<Attendance>> call = service.create(userId);
        call.enqueue(callback);
    }



}
