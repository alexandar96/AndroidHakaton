package cyberwarriors.hackathon.models;

import cyberwarriors.hackathon.interfaces.CategoryInterface;
import cyberwarriors.hackathon.interfaces.LessonInterface;
import cyberwarriors.hackathon.setup.Rest;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by Aleksandar on 12/11/2017.
 */

public class Category {

    public String id_category;

    public String name;

    public String logo;

    public static void getCategories(Callback<RestResponse<Category>> callback)
    {
        Retrofit retrofit = Rest.getRetrofit();
        CategoryInterface service = retrofit.create(CategoryInterface.class);
        Call<RestResponse<Category>> call = service.list();
        call.enqueue(callback);
    }

}
