package cyberwarriors.hackathon.models;

import cyberwarriors.hackathon.interfaces.LessonInterface;
import cyberwarriors.hackathon.interfaces.TopicInterface;
import cyberwarriors.hackathon.setup.Rest;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class Lesson {

    private String id_lesson;

    private String title;

    private String status;

    public String getId(){return id_lesson;}

    public String getTitle(){return title;}

    public String getStatus(){return status;}

    public Lesson(String id, String title)
    {
        id_lesson = id;
        this.title = title;

    }

    public static void loadLessonsBy(String topicId, Callback<RestResponse<Lesson>> callback)
    {
        Retrofit retrofit = Rest.getRetrofit();
        LessonInterface service = retrofit.create(LessonInterface.class);
        Call<RestResponse<Lesson>> call = service.listLesson(topicId);
        call.enqueue(callback);
    }

    public static void submitAttendanceCode(String userId, String lessonId, String Ip,String code, Callback<RestResponse<Lesson>> callback)
    {
        Retrofit retrofit = Rest.getRetrofit();
        LessonInterface service = retrofit.create(LessonInterface.class);
        Call<RestResponse<Lesson>> call = service.submitCode(userId,lessonId,Ip,code);
        call.enqueue(callback);
    }

    @Override
    public String toString(){return title;}
}
