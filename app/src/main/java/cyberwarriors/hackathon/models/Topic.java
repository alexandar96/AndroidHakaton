package cyberwarriors.hackathon.models;

import cyberwarriors.hackathon.interfaces.TopicInterface;
import cyberwarriors.hackathon.setup.Rest;
import cyberwarriors.hackathon.setup.RestResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;


public class Topic {

    private String id_topic;

    private String name;

    private String status;

    public String getId(){return id_topic;}

    public String getTopicName(){return name;}

    public String getStatus(){return status;}

    public Topic(String id, String name)
    {
        this.id_topic = id;
        this.name = name;
    }
    public static void loadTopics(Callback<RestResponse<Topic>> callback)
    {
        Retrofit retrofit = Rest.getRetrofit();
        TopicInterface service = retrofit.create(TopicInterface.class);
        Call<RestResponse<Topic>> call = service.list();
        call.enqueue(callback);

    }
    public static void loadTopicsFromCategory(String category_id,Callback<RestResponse<Topic>> callback)
    {
        Retrofit retrofit = Rest.getRetrofit();
        TopicInterface service = retrofit.create(TopicInterface.class);
        Call<RestResponse<Topic>> call = service.listFromCategory(category_id);
        call.enqueue(callback);

    }

    @Override
    public String toString(){return name;}
}
