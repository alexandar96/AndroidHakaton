package cyberwarriors.hackathon.models;

import cyberwarriors.hackathon.interfaces.UserInterface;
import cyberwarriors.hackathon.setup.Rest;
import cyberwarriors.hackathon.setup.RestResponse;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class User {

    private String username;

    private String password;

    private String id;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getId(){return id;}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static void login(String username, String password,Callback<RestResponse<User>> callbacks) {
        Retrofit retrofit = Rest.getRetrofit();
        UserInterface service = retrofit.create(UserInterface.class);
        Call<RestResponse<User>> call = service.create(username,password);
        call.enqueue(callbacks);
    }
}
