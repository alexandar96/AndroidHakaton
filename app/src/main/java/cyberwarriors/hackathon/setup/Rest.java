package cyberwarriors.hackathon.setup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cyberwarriors.hackathon.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Rest {

    public static Retrofit getRetrofit()
    {
        return Rest.getRetrofit(Globals.REST_SERVER);
    }

    public static Retrofit getRetrofit(String baseUrl)
    {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // Add the interceptor to OkHttpClient
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Rest.addLogger(builder);
        OkHttpClient client = builder.build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

    }

    private static void addLogger(OkHttpClient.Builder builder)
    {
        if (BuildConfig.DEBUG)
        {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.interceptors().add(interceptor);
        }
    }
}
