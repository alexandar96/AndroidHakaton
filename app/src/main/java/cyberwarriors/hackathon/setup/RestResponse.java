package cyberwarriors.hackathon.setup;

import java.util.List;

public class RestResponse<T> {

    private T data;

    public List<T> topics;

    public List<T> lessons;

    public List<T> category;

    public T getData(){return data;}
}
